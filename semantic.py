#!/usr/bin/python
#
#
# Title: Compulsury Task 1
#
# Usage: python semantic.py
# 
#####################################################################################
import spacy  # importing spacy
nlp = spacy.load('en_core_web_md')

notes = ["cat", "monkey", "banana"]
for token in notes:
    token = nlp(token)
    for token_ in notes:
        token_ = nlp(token_)
        print(token, "and", token_,token.similarity(token_))


# From running the code I can see there is more correlation between cat and monkey
# rather than banana and monkey. 
# as expected ech token with itself has a correlation of 100% 

# Example of my own is the following: 


notes2 = ["cat", "dog", "apple"]
for token in notes2:
    token = nlp(token)
    for token_ in notes2:
        token_ = nlp(token_)
        print(token, "and", token_,token.similarity(token_))


# Running example.py using en_core_web_sm rather than en_core_web_md
# lead to the following differences

# the correlation is lower for en_core_web_sm compared with the model en_core_web_md 
# for all the examples in example.py
